import React from "react";
import { Outlet } from "react-router-dom";

import Header from "./Header/Header";
import Footer from "./Footer/Footer";

function Layout({ favoriteIds, cart }) {
  return (
    <>
      <Header favorites={favoriteIds} cart={cart} />

      <main>
        <Outlet />
      </main>

      <Footer />
    </>
  );
}

export default Layout;
