import React from "react";
import "./App.css";
import Modal from "./components/Modal/Modal.js";
import Button from "./components/Button/Button.js";

class App extends React.Component {
  state = {
    modalFirstOpen: false,
    modalSecondOpen: false,
  };

  openFirstModal = () => {
    this.setState((prevState) => ({
      ...prevState,
      modalFirstOpen: !prevState.modalFirstOpen,
    }));
  };

  openSecondModal = () => {
    this.setState((prevState) => ({
      ...prevState,
      modalSecondOpen: !prevState.modalSecondOpen,
    }));
  };

  bodyClickHandler = (event) => {
    if (event.target.closest(".modal__content")) {
      return;
    }

    this.setState({
      modalFirstOpen: false,
      modalSecondOpen: false,
    });
  };

  render() {
    return (
      <React.Fragment>
        <div className="btn__wrapper">
          <Button
            onClick={this.openFirstModal}
            backgroundColor={"rgba(0, 0, 0, 0.8)"}
            text={"Open first modal"}
          />
          <Button
            onClick={this.openSecondModal}
            text={"Open second modal"}
            backgroundColor={"rgb(230, 67, 67)"}
          />
        </div>
        {this.state.modalFirstOpen && (
          <Modal
            closeButton={this.openFirstModal}
            header={"Do you want to delete this file?"}
            text={
              "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?"
            }
            actions={
              <>
                <div className="actions__wrapper">
                  <button className="actions__btn">Ok</button>
                  <button className="actions__btn">Cancel</button>
                </div>
              </>
            }
            bodyClickHandler={this.bodyClickHandler}
          />
        )}
        {this.state.modalSecondOpen && (
          <Modal
            closeButton={this.openSecondModal}
            header={"Do you want to add this product?"}
            text={"Are you sure you want to add it?"}
            actions={
              <>
                <div className="actions__wrapper">
                  <button className="actions__btn-second">Ok</button>
                  <button className="actions__btn-second">Cancel</button>
                </div>
              </>
            }
            bodyClickHandler={this.bodyClickHandler}
          />
        )}
      </React.Fragment>
    );
  }
}

export default App;
