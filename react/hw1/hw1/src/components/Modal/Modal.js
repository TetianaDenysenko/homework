import React from "react";

import "./Modal.scss";

class Modal extends React.Component {
  render() {
    const { header, closeButton, text, actions, bodyClickHandler } = this.props;
    return (
      <div className="modal" onClick={bodyClickHandler}>
        <div className="modal__content">
          <div className="modal__header-wrapper">
            <h2 className="modal__header-text">{header}</h2>
            <button type="button" className="btn__close" onClick={closeButton}>
              X
            </button>
          </div>
          <p className="modal__text">{text}</p>
          {actions}
        </div>
      </div>
    );
  }
}

export default Modal;
