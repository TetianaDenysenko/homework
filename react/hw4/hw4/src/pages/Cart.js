import React from "react";
import CardList from "../components/CardList/CardList";

function Cart({
  products,
  favorites,
  onAddToCart,
  onAddToFavorites,
  onMinusProduct,
  onPlusProduct,
}) {
  return (
    <>
      <CardList
        products={products}
        favorites={favorites}
        onAddToFavorites={onAddToFavorites}
        onAddToCart={onAddToCart}
        onMinusProduct={onMinusProduct}
        onPlusProduct={onPlusProduct}
      />
    </>
  );
}

export default Cart;
