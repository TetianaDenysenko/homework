import React from "react";
import PropTypes from "prop-types";

import "./Header.scss";

class Header extends React.Component {
  static defaultProps = {
    favorites: [],
    cart: [],
  };

  render() {
    const { favorites, cart } = this.props;
    return (
      <header className="header">
        <div>
          <a href="/">
            <img src="./icons/apple.svg" alt="apple" width="35" height="35" />
          </a>
        </div>
        <div className="header__content">
          <button type="button" className="header__btn">
            <img src="./icons/star.svg" alt="star" />
            {favorites.length > 0 && (
              <span className="header__counters">{favorites.length}</span>
            )}
          </button>
          <button type="button" className="header__btn">
            <img src="./icons/shop.svg" alt="cart" className="header__basket" />
            {cart.length > 0 && (
              <span className="header__counters">{cart.length}</span>
            )}
          </button>
        </div>
      </header>
    );
  }
}

Header.propTypes = {
  favorites: PropTypes.array,
  cart: PropTypes.array,
};

export default Header;
