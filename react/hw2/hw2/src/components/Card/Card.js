import React from "react";
import PropTypes from "prop-types";

import star from "./star.svg";
import staryellow from "./staryellow.svg";
import Modal from "../Modal/Modal.js";
import Button from "../Button/Button.js";

import "./Card.scss";

class Card extends React.Component {
  static defaultProps = {
    products: [],
    favorites: [],
  };

  state = {
    modalFirstOpen: false,
  };

  openFirstModal = () => {
    this.setState((prevState) => ({
      ...prevState,
      modalFirstOpen: !prevState.modalFirstOpen,
    }));
  };

  bodyClickHandler = (event) => {
    if (event.target.closest(".modal__content")) {
      return;
    }

    this.setState({
      modalFirstOpen: false,
    });
  };

  render() {
    const { product, favorites, onAddToFavorites, onAddToCart } = this.props;

    return (
      <li className="product-list__item">
        <div>
          <img src={product.photo} alt="phone" className="product-list__img" />
        </div>
        <div>
          <p className="product-list__title">{product.title}</p>
          <p>Color: {product.color}</p>
          <p>Article: {product.article}</p>
          <div className="product-list__buy">
            <p className="product-list__price">{product.price}$</p>
            <button
              className="product-list__add-to-fav"
              onClick={() => onAddToFavorites(product.id)}
            >
              <img
                src={favorites.includes(product.id) ? staryellow : star}
                alt="star"
                className="product-list__star"
              />
            </button>
            <div className="btn__wrapper">
              <Button
                onClick={this.openFirstModal}
                backgroundColor={"rgba(0, 0, 0, 0.8)"}
                text={"Add to cart"}
              />
            </div>
            {this.state.modalFirstOpen && (
              <Modal
                closeButton={this.openFirstModal}
                text={"Do you want to add this produtc to card?"}
                actions={
                  <>
                    <div className="actions__wrapper">
                      <button
                        className="actions__btn"
                        onClick={() => {
                          onAddToCart(product.id);
                          this.openFirstModal();
                        }}
                      >
                        Ok
                      </button>
                      <button
                        className="actions__btn"
                        onClick={() => {
                          this.openFirstModal();
                        }}
                      >
                        Cancel
                      </button>
                    </div>
                  </>
                }
                bodyClickHandler={this.bodyClickHandler}
              />
            )}
          </div>
        </div>
      </li>
    );
  }
}

Card.propTypes = {
  products: PropTypes.array,
  favorite: PropTypes.array,
  onAddToFavorites: PropTypes.func,
  onAddToCart: PropTypes.func,
};

export default Card;
