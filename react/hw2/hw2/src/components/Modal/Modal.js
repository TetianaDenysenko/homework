import React from "react";
import PropTypes from "prop-types";

import "./Modal.scss";

class Modal extends React.Component {
  static defaultProps = {
    product: [],
    favorites: [],
  };

  render() {
    const { closeButton, text, actions, bodyClickHandler } = this.props;
    return (
      <div className="modal" onClick={bodyClickHandler}>
        <div className="modal__content">
          <div className="modal__header-wrapper">
            <button type="button" className="btn__close" onClick={closeButton}>
              X
            </button>
          </div>
          <p className="modal__text">{text}</p>
          {actions}
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  product: PropTypes.array,
  favorite: PropTypes.array,
  closeButton: PropTypes.func,
  text: PropTypes.string,
  actions: PropTypes.element,
  bodyClickHandler: PropTypes.func,
};

export default Modal;
