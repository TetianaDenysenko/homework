import React from "react";

import "./Button.scss";

class Button extends React.Component {
  render() {
    const { onClick, backgroundColor, text } = this.props;
    return (
      <button
        type="button"
        className="btn"
        style={{ backgroundColor: backgroundColor }}
        onClick={onClick}
      >
        {text}
      </button>
    );
  }
}

export default Button;
