import React from "react";
import Card from "../Card/Card";

class CardList extends React.Component {
  render() {
    const { products, favorites, onAddToFavorites, onAddToCart } = this.props;
    return (
      <>
        <div className="product-list__container">
          <ul className="product-list">
            {products.map((product) => {
              return (
                <Card
                  key={product.id}
                  product={product}
                  favorites={favorites}
                  onAddToFavorites={onAddToFavorites}
                  onAddToCart={onAddToCart}
                />
              );
            })}
          </ul>
        </div>
      </>
    );
  }
}

export default CardList;
