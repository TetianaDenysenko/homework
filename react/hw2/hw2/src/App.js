import React from "react";

import "./App.css";
import Footer from "./components/Footer/Footer";
import Header from "./components/Header/Header";
import CardList from "./components/CardList/CardList";

class App extends React.Component {
  state = {
    products: [],
    favorites: [],
    cart: [],
  };

  componentDidMount() {
    fetch(`./products.json`)
      .then((res) => {
        return res.json();
      })
      .then((data) => this.setState({ products: data }));

    this.setState((prev) => ({
      ...prev,
      favorites: JSON.parse(localStorage.getItem("favorites")) || [],
    }));

    this.setState((prev) => ({
      ...prev,
      cart: JSON.parse(localStorage.getItem("cart")) || [],
    }));
  }

  handleAddToFavorites = (productId) => {
    if (!this.state.favorites.includes(productId)) {
      this.setState((prev) => ({
        ...prev,
        favorites: [...prev.favorites, productId],
      }));

      localStorage.setItem(
        "favorites",
        JSON.stringify([...this.state.favorites, productId])
      );
    } else {
      this.setState((prev) => ({
        ...prev,
        favorites: prev.favorites.filter((id) => id !== productId),
      }));

      localStorage.setItem(
        "favorites",
        JSON.stringify([
          ...this.state.favorites.filter((id) => id !== productId),
        ])
      );
    }
  };

  handleAddToCart = (id) => {
    const cartArray = [...this.state.cart];

    if (cartArray.some((product) => product.id === id)) {
      let updatedCart = cartArray.map((product) => {
        if (product.id === id) {
          return {
            ...product,
            quantity: product.quantity + 1,
          };
        }
        return product;
      });

      this.setState((prev) => ({
        ...prev,
        cart: updatedCart,
      }));

      localStorage.setItem("cart", JSON.stringify(updatedCart));
    } else {
      cartArray.push({ id, quantity: 1 });
      this.setState((prev) => ({
        ...prev,
        cart: cartArray,
      }));

      localStorage.setItem("cart", JSON.stringify(cartArray));
    }
  };

  render() {
    const { products } = this.state;
    return (
      <>
        <Header favorites={this.state.favorites} cart={this.state.cart} />
        <main>
          <CardList
            products={products}
            favorites={this.state.favorites}
            onAddToFavorites={this.handleAddToFavorites}
            onAddToCart={this.handleAddToCart}
          />
        </main>
        <Footer />
      </>
    );
  }
}

export default App;
