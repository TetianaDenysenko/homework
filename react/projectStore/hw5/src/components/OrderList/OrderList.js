import React from "react";
import "./OrderList.scss";

function OrderList({ products, onAddToCart }) {
  const cart = JSON.parse(localStorage.getItem("cart")) || [];

  const cartDetails = cart.map((item) => {
    if (products.find((product) => product.id === item.id)) {
      return {
        ...products.find((product) => product.id === item.id),
        quantity: item.quantity,
      };
    }

    return item;
  });

  const totalPrice = cartDetails.reduce((sum, curr) => {
    return sum + Number(curr.price.replace(" ", "")) * Number(curr.quantity);
  }, 0);

  return (
    <>
      <ul className="order-list">
        {cartDetails.map((product) => {
          return (
            <li key={product.id} className="order-list__item">
              <img
                src={product.photo}
                alt="phone"
                className="order-list__img"
              />
              <p>
                {product.title}, {product.quantity}, {product.price}$
              </p>

              <button onClick={() => onAddToCart(product.id, "delete")}>
                x
              </button>
            </li>
          );
        })}
      </ul>
      <div>
        Total price: ${totalPrice} for total {cart.length} items
      </div>
    </>
  );
}

export default OrderList;
