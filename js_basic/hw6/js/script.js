"use strict";
function createNewUser(){
    let firstName = prompt("What is your first name?");
    let lastName = prompt("What is your last name?");
    let birthday = prompt("Your birthday dd.mm.yyyy:")
    const newUser = {
        firstName,
        lastName,
        birthday,
        getLogin:() => {
            return newUser.firstName.toString().toLowerCase()[0] + newUser.lastName.toString().toLowerCase();
        },
        getAge:() => {
            let now = new Date();
            let today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
            let userDate = new Date(newUser.birthday);
            let userBirthday = new Date(today.getFullYear(), userDate.getMonth(), userDate.getDate());
            let age = today.getFullYear() - userDate.getFullYear();
            if (today < userBirthday) {
                age = age-1;
              };
            return age;
        },
        getPassword:() => {
            return newUser.firstName.toString().toUpperCase()[0] + newUser.lastName.toString().toLowerCase() + newUser.birthday.substring(6,10);
        },
    };
    return newUser;
};
const user = createNewUser();
const userLogin = user.getLogin();
const userAge = user.getAge();
const userPassword = user.getPassword();
console.log(userLogin);
console.log(userAge);
console.log(userPassword);