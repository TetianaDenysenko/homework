"use strict";

let tab = function () {
  let tabsTitle = document.querySelectorAll(".tabs-title");
  let tabsContent = document.querySelectorAll(".tab");

  tabsTitle.forEach((item) => {
    item.addEventListener("click", selectTabsTitle);
  });

  function selectTabsTitle() {
    tabsTitle.forEach((item) => {
      item.classList.remove("active");
    });

    this.classList.add("active");
    let tabName = this.getAttribute("data-tab-name");
    selectTabsContent(tabName);
  }

  function selectTabsContent(tabName) {
    tabsContent.forEach((item) => {
      item.classList.contains(tabName)
        ? item.classList.add("active")
        : item.classList.remove("active");
    });
  }
};

tab();
