"use strict";

const images = document.querySelectorAll(".image");
const startButton = document.querySelector(".start");
const stopButton = document.querySelector(".stop");
let timer = setInterval(() => cycleImage(images), 3000);
let currentIndex = [...images].findIndex((image) =>
  image.classList.contains("image-to-show")
);

startButton.addEventListener("click", () => {
  clearInterval(timer);
  timer = setInterval(() => cycleImage(images), 3000);
});

stopButton.addEventListener("click", () => clearInterval(timer));
function cycleImage(images) {
  let next;
  let curr = currentIndex;
  if (curr === images.length - 1) {
    next = 0;
  } else {
    next = curr + 1;
  }
  images[curr].classList.toggle("image-to-show");
  images[next].classList.toggle("image-to-show");
  return (currentIndex = next);
}
