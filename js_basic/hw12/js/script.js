"use strict";
const body = document.body;
const buttons = document.querySelectorAll(".btn");
let currentKey;
body.addEventListener("keydown", assignCurrentKey);
function assignCurrentKey(event) {
  currentKey = event.key;
  classActive();
}
function classActive() {
  buttons.forEach((button) => button.classList.remove("active"));
  buttons.forEach((button) => {
    if (currentKey === "Enter") {
      if (button.innerHTML === currentKey) {
        button.classList.add("active");
      }
    } else {
      if (button.innerHTML.toLowerCase() === currentKey) {
        button.classList.add("active");
      }
    }
  });
}
