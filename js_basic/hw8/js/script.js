"use strict";

const paragraphs = document.querySelectorAll("p");
paragraphs.forEach(
  (paragraph) => (paragraph.style.backgroundColor = "#ff0000")
);

const optionsList = document.getElementById("optionsList");
console.log(optionsList);
const parent = optionsList.parentNode;
console.log(parent);
const optionsArray = [...optionsList.children];
optionsArray.forEach((child) => {
  console.log(`${child.nodeName}: ${child.nodeType}`);
});

const content = document.getElementById("testParagraph");
content.innerHTML = "<p>This is a paragraph</p>";

// const listClass = document.getElementsByClassName("main-header");
const listItems = document.querySelectorAll(".main-header li");
// const listItems = [...listClass[0].children[0].children[1].children];
console.log(listItems);
listItems.forEach((item) => (item.className = "nav-item"));

const classOption = [...document.getElementsByClassName("options-list-title")];
classOption.forEach((element) => {
  element.classList.remove("options-list-title");
});
