"use strict";

let num1;
let num2;
let mathOperation;

do{
    num1 = prompt('num1', num1 ? num1 : '');
}  while (num1 === '' || isNaN(num1) || num1 === null);
do{
    num2 = prompt('num2', num2 ? num2 : '');
} while (num2 === '' || isNaN(num2) || num2 === null);
do{
    mathOperation = prompt("Your operation is +, -, *, /?");
} while (mathOperation === null || mathOperation === '');

num1 = Number(num1);
num2 = Number(num2);

function summ (num1, num2, operator){
    switch (operator){
        case '+':
            return num1 + num2;
        case '-':
            return num1 - num2;
        case '*':
            return num1 * num2;
        case '/':
            return num1 / num2;
        default: alert('Error')
    };
};
console.log(summ(num1, num2, mathOperation));