"use strict";

function createList(arr, parent = document.body) {
  const list = document.createElement("ul");
  const listItems = arr
    .map((item) => {
      if (Array.isArray(item)) {
        const list = document.createElement("ul");
        const listItems = item.map((item) => `<li>${item}</li>`).join("");
        list.insertAdjacentHTML("beforeend", listItems);

        return list.outerHTML;
      }

      return `<li>${item}</li>`;
    })
    .join("");

  list.insertAdjacentHTML("beforeend", listItems);
  parent.append(list);
}

createList([
  "Kharkiv",
  "Kiev",
  ["Borispol", "Irpin"],
  "Odessa",
  "Lviv",
  "Dnieper",
]);
