"use strict";

const button = document.querySelector(".header-button");
const page = document.querySelector(".page");

button.addEventListener("click", changeTheme);

let currentTheme = localStorage.getItem("theme") || 'light-theme';
page.classList.add(currentTheme);

function changeTheme() {
  if (page.classList.contains("light-theme")) {
    page.classList.remove("light-theme");
    page.classList.add("dark-theme");
    localStorage.setItem("theme", "dark-theme");
  } else {
    page.classList.remove("dark-theme");
    page.classList.add("light-theme");
    localStorage.setItem("theme", "light-theme");
  }
}
