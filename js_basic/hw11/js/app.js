"use strict";
const form = document.querySelector(".password-form");
const inputs = document.getElementsByTagName("input");
const icons = document.querySelectorAll(".icon-password");
const btn = document.querySelector(".btn");

icons.forEach((icon) => icon.addEventListener("click", show));

function show(event) {
  const icon = event.target;
  const input = event.target.previousElementSibling;
  if (icon.classList.contains("fa-eye")) {
    icon.classList.remove("fa-eye");
    icon.classList.add("fa-eye-slash");
    input.type = "password";
  } else {
    icon.classList.remove("fa-eye-slash");
    icon.classList.add("fa-eye");
    input.type = "text";
  }
}

btn.addEventListener("click", cheсk);
function cheсk(event) {
  event.preventDefault();
  if (form.elements[0].value === form.elements[1].value) {
    alert("You are welcome");
  } else {
    alert("Нужно ввести одинаковые значения");
  }
}
