const burgerButton = document.querySelector(".burger-button");
const headerNav = document.querySelector(".header-menu-column");

burgerButton.addEventListener("click", headerMenuOpen);

function headerMenuOpen() {
  burgerButton.classList.toggle("burger-button--active");
  headerNav.classList.toggle("header-menu-column--open");
}
