import gulp from "gulp";
import { deleteAsync } from "del";
import browserSync from "browser-sync";
import dartSass from "sass";
import gulpSass from "gulp-sass";
import cleanCSS from "gulp-clean-css";
import concat from "gulp-concat";
import uglify from "gulp-uglify";
import sourcemaps from "gulp-sourcemaps";
import autoprefixer from "gulp-autoprefixer";
import imagemin from "gulp-imagemin";
import newer from "gulp-newer";

const sass = gulpSass(dartSass);
const bs = browserSync.create();

const scrFolder = "./src";
const buildFolder = "./dist";

const path = {
  build: {
    scripts: `${buildFolder}/js`,
    styles: `${buildFolder}/css/`,
    images: `${buildFolder}/img/`,
  },
  src: {
    scripts: `${scrFolder}/scripts/**/*.js`,
    styles: `${scrFolder}/styles/**/*.scss`,
    images: `${scrFolder}/img/**/*.{jpg,jpeg,png,svg,webp}`,
  },
  watch: {
    scripts: `${scrFolder}/scripts/**/*.js`,
    styles: `${scrFolder}/styles/**/*.scss`,
  },
};

const clean = () => {
  return deleteAsync(["./dist/*", "!./dist/img"]);
};

const scripts = () => {
  return gulp
    .src(path.src.scripts, {
      sourcemaps: true,
    })
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(concat("script.min.js"))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(path.build.scripts))
    .pipe(bs.stream());
};

const styles = () => {
  return gulp
    .src(path.src.styles)
    .pipe(sourcemaps.init())
    .pipe(sass().on("error", sass.logError))
    .pipe(
      autoprefixer({
        cascade: false,
      })
    )
    .pipe(cleanCSS())
    .pipe(concat("styles.min.css"))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(path.build.styles))
    .pipe(bs.stream());
};

const images = () => {
  return gulp
    .src(path.src.images)
    .pipe(newer(path.build.images))
    .pipe(imagemin())
    .pipe(gulp.dest(path.build.images));
};

const watch = () => {
  bs.init({
    server: {
      baseDir: "./",
    },
    browser: "google chrome",
    open: true,
  });
  gulp.watch("*.html").on("change", bs.reload);
  gulp.watch(path.src.styles, styles);
  gulp.watch(path.src.scripts, scripts);
  gulp.watch(path.src.images, images);
};

const dev = gulp.series(gulp.parallel(styles, scripts, images), watch);

const build = gulp.series(clean, gulp.parallel(styles, scripts, images));

gulp.task("dev", dev);
gulp.task("build", build);
