"use strict";

const BASE_URL = "https://api.ipify.org/?format=json";
const apiIp = "http://ip-api.com/json";
const root = document.querySelector(".root");

const button = document.querySelector(".button");

class GetIP {
  constructor(urlIp, urlInfo, root) {
    this.urlIp = urlIp;
    this.urlInfo = urlInfo;
    this.root = root;
  }

  async getIp() {
    try {
      const response = await fetch(this.urlIp);
      const { ip } = await response.json();
      return ip;
    } catch (error) {
      console.log(error);
    }
  }

  async getInfo() {
    try {
      const ip = await this.getIp();
      const response = await fetch(`${this.urlInfo}/${ip}`);
      const { country, countryCode, regionName, city } = await response.json();

      const list = document.createElement("ul");

      const countryItem = document.createElement("li");
      countryItem.innerText = `Country: ${country}`;

      const countrycode = document.createElement("li");
      countrycode.innerText = `Country code: ${countryCode}`;

      const regionItem = document.createElement("li");
      regionItem.innerText = `Region: ${regionName}`;

      const cityItem = document.createElement("li");
      cityItem.innerText = `City: ${city}`;

      list.append(countryItem, countrycode, regionItem, cityItem);

      root.insertAdjacentElement("beforeend", list);
    } catch (error) {
      console.log(error);
    }
  }
}

const getIpInfo = new GetIP(BASE_URL, apiIp, root);

button.addEventListener("click", () => getIpInfo.getInfo());
