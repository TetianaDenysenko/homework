"use strict";

const url = "https://ajax.test-danit.com/api/swapi/";
const root = document.getElementById("root");

class Films {
  constructor(main_url, root) {
    this.main_url = url;
    this.root = root;
  }

  render() {
    this.request(this.main_url, "films/").then((films) => {
      const sortedFilms = films.sort((x, y) => x.episodeId - y.episodeId);

      const filmList = document.createElement("ul");

      sortedFilms.map((film) => {
        const filmItem = document.createElement("li");

        const filmTitle = document.createElement("h2");
        filmTitle.innerText = film.name;

        const filmEpisode = document.createElement("p");
        filmEpisode.innerText = `Episode: ${film.episodeId}`;

        const filmDescription = document.createElement("p");
        filmDescription.innerText = film.openingCrawl;

        const characterList = document.createElement("ul");

        const load = document.createElement("div");
        load.classList.add("load");

        filmItem.append(filmTitle, filmEpisode, filmDescription, load);

        const characterPromises = film.characters.map((link) => {
          return this.request(link);
        });

        Promise.all(characterPromises).then((characters) => {
          const charactersArray = characters.map((character) => {
            const characterItem = document.createElement("li");
            characterItem.innerText = character.name;

            return characterItem;
          });

          load.remove();
          characterList.append(...charactersArray);
        });

        filmItem.append(characterList);

        filmList.append(filmItem);
        this.root.append(filmList);
      });
    });
  }

  request(link, collection = "") {
    return fetch(link + collection, {
      method: "GET",
      headers: {
        "Content-type": "application/json",
      },
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error(`${response.status} - ${response.statusText}`);
        }

        return response.json();
      })
      .catch((error) => {
        console.log(error);
      });
  }
}

const films = new Films(url, root);
films.render();
