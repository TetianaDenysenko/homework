"use strict";

const url = "https://ajax.test-danit.com/api/json/";
const root = document.querySelector("#root");
const loader = document.querySelector(".lds-roller");

const addPostBtn = document.createElement("button");
addPostBtn.className = "add-post--btn";
addPostBtn.textContent = "Add new post";
addPostBtn.addEventListener("click", () => {
  addPostForm.classList.remove("hidden");
  document.body.classList.add("body-hidden");
});

const addPostForm = document.createElement("form");
addPostForm.classList.add("add-post--form", "hidden");

const newPostFormWrapper = document.createElement("div");
newPostFormWrapper.className = "add-post--form-wrapper";

const newTitleInputLabel = document.createElement("label");

newTitleInputLabel.textContent = "Enter new post title";
newTitleInputLabel.htmlFor = "new-post-title";

const newTitleInput = document.createElement("input");
newTitleInput.className = "new-post__title";
newTitleInput.id = "new-post__title";

const newTextInputLabel = document.createElement("label");
newTextInputLabel.htmlFor = "new-post__text";
newTextInputLabel.textContent = "Enter text";

const newTextInput = document.createElement("input");
newTextInput.className = "new-post__text";
newTextInput.id = "new-post__text;";

const newPostSubmitBtn = document.createElement("button");
newPostSubmitBtn.className = "new-post__submit";
newPostSubmitBtn.textContent = "Add new post";

newPostSubmitBtn.addEventListener("click", (event) => {
  event.preventDefault();

  if (newTitleInput.value.trim() === "" || newTextInput.value.trim() === "") {
    return;
  } else {
    //?
    addPostForm.classList.add("hidden");
    document.body.classList.remove("body-hidden");
    cards.addCard(newTitleInput.value.trim(), newTextInput.value.trim());
    newTitleInput.value = "";
    newTextInput.value = "";
  }
});

addPostForm.append(newPostFormWrapper);
newPostFormWrapper.append(
  newTitleInputLabel,
  newTitleInput,
  newTextInputLabel,
  newTextInput,
  newPostSubmitBtn
);

root.append(addPostForm, addPostBtn);

class EditForm {
  constructor(title, text, currentCard) {
    this.editForm = document.createElement("form");
    this.editForm.className = "card__edit-form";
    this.editFormTitle = document.createElement("input");
    this.editFormTitle.className = "edit-form__title";
    this.editFormTitle.style.fontSize = "24px";
    this.editFormTitle.style.fontWeight = "700";

    this.editFormTitle.value = title;

    this.editFormText = document.createElement("textarea");
    this.editFormText.rows = 4;
    this.editFormText.className = "edit-form__text";
    this.editFormText.style.fontSize = "15px";
    this.editFormText.style.fontFamily = "Arial";
    this.editFormText.value = text;

    this.saveButton = document.createElement("button");
    this.saveButton.className = "card__save-btn";
    this.saveButton.textContent = "Save changes";

    //?
    currentCard.querySelector(".card__edit-btn").classList.add("hidden-btn");
    currentCard.querySelector(".card__button-wrapper").prepend(this.saveButton);

    this.editForm.append(this.editFormTitle, this.editFormText);
  }
}

class Requests {
  constructor(url) {
    this.base_url = url;
  }

  request(entity = "", id = "", method = "GET", data) {
    const options = {
      method,
      headers: {
        "Content-type": "application/json;charset=UTF-8",
      },
    };

    if (data) {
      options.body = JSON.stringify(data);
    }

    return fetch(`${this.base_url}${entity}/${id}`, options)
      .then((response) => {
        if (!response.ok) {
          throw new Error(`${response.status} - ${response.statusText}`);
        }

        if (method === "DELETE") {
          return response;
        }

        return response.json();
      })
      .catch((error) => console.log(error));
  }

  getData(entity, id = "") {
    return this.request(entity, id);
  }

  postData(entity, id = "", data) {
    return this.request(entity, id, "POST", data);
  }

  deleteData(entity, id) {
    return this.request(entity, id, "DELETE");
  }

  putData(entity, id, data) {
    return this.request(entity, id, "PUT", data);
  }
}

const requests = new Requests(url);

class Card {
  constructor(post) {
    this.card = document.createElement("div");
    this.card.className = "card";
    this.card.setAttribute("data-id", post.id);
    // this.card.dataset.id = post.id;

    this.cardTitle = document.createElement("h2");
    this.cardTitle.className = "card__title";
    this.cardTitle.textContent = post.title;

    this.cardText = document.createElement("p");
    this.cardText.className = "card__text";
    this.cardText.textContent = post.body;

    this.userName = document.createElement("p");
    this.userName.className = "card__user-name";
    this.userName.textContent = post.userName;

    this.userEmail = document.createElement("p");
    this.userEmail.className = "card__user-email";
    this.userEmail.textContent = post.userEmail;

    this.deleteCardBtn = document.createElement("button");
    this.deleteCardBtn.className = "card__delete-btn";
    this.deleteCardBtn.textContent = "x";
    this.deleteCardBtn.addEventListener("click", (event) => {
      //?
      cards.deleteCard(event.target.closest(".card").dataset.id);
    });

    this.editCardBtn = document.createElement("button");
    this.editCardBtn.className = "card__edit-btn";
    this.editCardBtn.textContent = "Edit";
    this.editCardBtn.addEventListener("click", (event) => {
      cards.editCard(event.target.closest(".card").dataset.id);
    });

    this.buttonWrapper = document.createElement("div");
    this.buttonWrapper.className = "card__button-wrapper";
    this.buttonWrapper.append(this.editCardBtn, this.deleteCardBtn);

    this.card.append(
      this.cardTitle,
      this.cardText,
      this.userName,
      this.userEmail,
      this.buttonWrapper
    );
  }
}

class Cards {
  constructor(root) {
    this.root = root;
  }

  getUsersWithPosts = async () => {
    this.users = await requests.getData("users");
    this.posts = await requests.getData("posts");
    const usersWithPosts = this.posts.map((post) => {
      const newPost = { ...post };

      this.users.forEach((user) => {
        if (user.id === post.userId) {
          newPost.name = user.name;
          newPost.email = user.email;
        }
      });

      return newPost;
    });

    this.visiblePosts = usersWithPosts.map((post) => {
      const newCard = new Card(post);

      return newCard.card;
    });

    this.cardContent = document.createElement("div");
    this.cardContent.className = "card__content";

    this.root.append(this.cardContent);
    this.cardContent.append(...this.visiblePosts);
    loader.remove();
  };

  render() {
    this.getUsersWithPosts();
  }

  deleteCard(cardId) {
    console.log(cardId, "this is deleteCard");
    this.cardContent.innerHTML = "";
    this.visiblePosts = this.visiblePosts.filter(
      (card) => +card.dataset.id !== +cardId
    );
    this.cardContent.append(...this.visiblePosts);

    requests
      .deleteData("posts", +cardId)
      .then((response) => {
        if (!response.ok) {
          throw new Error(`${response.status}`);
        }

        return console.log("The post has been deleted successfully");
      })
      .catch((err) => console.log(err));
  }

  addCard(title, body) {
    const newPost = {
      id: 1,
      title,
      body,
      userId: 1,
      userName: this.users.find((user) => user.id === 1).name,
      userEmail: this.users.find((user) => user.id === 1).email,
    };

    const newCard = new Card(newPost);
    this.visiblePosts.forEach((card) => ++card.dataset.id);
    console.log(this.visiblePosts);
    this.visiblePosts.unshift(newCard.card);

    requests.postData("posts", "", newPost);
    this.cardContent.innerHTML = "";
    this.cardContent.append(...this.visiblePosts);
    // 1 создаем новую карточку
    // 2 перебираем датасет ид и увеличиваем на 1
    // 3 добавляем в начало массива карточек новую карточку
    // 4 оптравляем гет запрос
    // 5 очищаем контейнер карточек
    // 6 рендерим новый список карточек на основе обновленного массива карточек,
    // как вариант можно отправить новый гет запрос, получить обновленные данные и на их основе
    // отрисовать карточки заново
  }

  editCard(cardId) {
    console.log(cardId);
    const currentCard = this.visiblePosts.find(
      (card) => card.dataset.id === cardId
    );
    const currentCardTitle = currentCard.querySelector(".card__title");
    const currentCardText = currentCard.querySelector(".card__text");
    const currentCardEditBtn = currentCard.querySelector(".card__edit-btn");
    currentCardEditBtn.classList.add("hidden-btn");
    currentCard.style.background = "lightblue";

    currentCardTitle.classList.add("hidden-field");
    currentCardText.classList.add("hidden-field");

    const editForm = new EditForm(
      currentCardTitle.textContent,
      currentCardText.textContent,
      currentCard
    );

    currentCard.prepend(editForm.editForm);

    currentCard
      .querySelector(".card__save-btn")
      .addEventListener("click", (event) => {
        console.log(event.target.closest(".edit-form__title"));
        const titleInput = currentCard.querySelector(".edit-form__title");
        const textInput = currentCard.querySelector(".edit-form__text");

        if (titleInput.value !== currentCardTitle.textContent) {
          currentCardTitle.textContent = titleInput.value;
        }

        if (textInput.value !== currentCardText.textContent) {
          currentCardText.textContent = textInput.value;
        }

        const data = {
          title: titleInput.value,
          body: textInput.value,
        };

        console.log(data);

        requests
          .putData("posts", currentCard.dataset.id, data)
          .then(console.log);

        currentCardTitle.classList.remove("hidden-field");
        currentCardText.classList.remove("hidden-field");
        document.querySelector(".card__edit-form").remove();
        document.querySelector(".card__save-btn").remove();
        document
          .querySelector(".card__edit-btn")
          .classList.remove("hidden-btn");
        currentCard.style.background = "lightgoldenrodyellow";
      });
  }
}

const cards = new Cards(root);
cards.render();
