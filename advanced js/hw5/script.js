"use strict";

const base_url = "https://ajax.test-danit.com/api/json/";
const root = document.querySelector("#root");

class Requests {
  constructor(url) {
    this.base_url = url;
  }

  request(entity = "", id = "", method = "GET", data) {
    const options = {
      method,
      headers: {
        "Content-type": "application/json",
      },
    };

    if (data) {
      options.body = JSON.stringify(data);
    }

    return fetch(`${this.base_url}${entity}/${id}`, options)
      .then((response) => {
        if (!response.ok) {
          throw new Error(`${response.status}`);
        }

        if (method === "DELETE") {
          return response;
        }

        return response.json();
      })
      .catch((error) => console.log(error));
  }

  getData(entity = "") {
    return this.request(entity);
  }

  deleteData(entity, id) {
    return this.request(entity, id, "DELETE");
  }

  putData(entity, id, data) {
    return this.request(entity, id, "PUT", data);
  }

  postData(entity, id = "", data) {
    return this.request(entity, id, "POST", data);
  }
}

const requests = new Requests(base_url);

class Cards {
  constructor(root) {
    this.root = root;
  }

  getUserWithPosts = async () => {
    const posts = await requests.getData("posts");
    const users = await requests.getData("users");

    this.postsWithUsers = posts.map((post) => {
      const newPost = { ...post };

      users.forEach((user) => {
        if (user.id === post.userId) {
          newPost.name = user.name;
          newPost.email = user.email;
        }
      });

      return newPost;
    });
  };

  deleteCard(card) {
    const cardId = card.dataset.id;

    requests
      .deleteData("posts", +cardId)
      .then((response) => {
        if (!response.ok) {
          throw new Error(`${response.status}`);
        }

        return console.log("The post has been deleted successfully");
      })
      .catch((err) => console.log(err));
    this.visiblePosts = this.visiblePosts.filter((post) => post.id !== cardId);
    card.remove();
  }

  render() {
    this.getUserWithPosts().then(() => {
      this.visiblePosts = this.postsWithUsers.map((post) => {
        const card = new Card(post);
        return card.card;
      });

      this.root.append(...this.visiblePosts);
    });
  }
}

class Card {
  constructor(post) {
    this.card = document.createElement("div");
    this.card.className = "card";
    this.card.dataset.id = post.id;

    this.cardUserName = document.createElement("p");
    this.cardUserName.className = "card__userName";
    this.cardUserName.textContent = post.name;

    this.cardUserEmail = document.createElement("p");
    this.cardUserEmail.className = "card__userEmail";
    this.cardUserEmail.textContent = post.email;

    this.cardTitle = document.createElement("p");
    this.cardTitle.className = "card__title";
    this.cardTitle.textContent = post.title;

    this.cardText = document.createElement("p");
    this.cardText.textContent = post.body;

    this.cardDeleteButton = document.createElement("button");
    this.cardDeleteButton.className = "card__delete-btn";
    this.cardDeleteButton.textContent = "X";
    this.cardDeleteButton.addEventListener("click", (event) => {
      const card = event.target.closest("div");
      cards.deleteCard(card);
    });

    this.wrapper = document.createElement("div");
    this.wrapper.className = "wrapper";

    this.wrapper.append(this.cardUserName, this.cardUserEmail);
    this.card.append(
      this.wrapper,
      this.cardTitle,
      this.cardText,
      this.cardDeleteButton
    );
  }
}

const cards = new Cards(root);

cards.render();
