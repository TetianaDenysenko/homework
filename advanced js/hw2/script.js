"use strict";

const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

class List {
  constructor() {}

  render(root) {
    this.list = document.createElement("ul");

    const createLi = function () {
      const listItems = books.map((book) => {
        try {
          if (!book.name || !book.price || !book.author) {
            throw new Error("This property is not present");
          } else {
            const li = `
            <li>
              <h2>${book.name}</h2>
              <p>${book.author}</p>
              <p>${book.price}</p>
            </li>`;

            return li;
          }
        } catch (error) {
          console.log(error);
        }
      });

      return listItems.filter((item) => item);
    };

    this.items = createLi().join("");

    this.list.insertAdjacentHTML("beforeend", this.items);
    root.append(this.list);
  }
}
const list = new List(books);
const root = document.querySelector("#root");
list.render(root);
