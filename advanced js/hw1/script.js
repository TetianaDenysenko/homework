"use strict";

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  set name(value) {
    this._name = value;
  }

  get name() {
    return this._name;
  }

  set age(value) {
    this._age = value;
  }

  get age() {
    return this._age;
  }

  set salary(value) {
    this._salary = value;
  }

  get salary() {
    return this._salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }

  set lang(value) {
    this._lang = value;
  }

  get lang() {
    return this._lang;
  }

  set salary(value) {
    this._salary = value;
  }

  get salary() {
    return this._salary * 3;
  }
}

const employee = new Employee("Alex", 25, 3000);
const programmer = new Programmer("Andrew", 30, 6000, "Russian, English");
const programmer1 = new Programmer(
  "Michael",
  35,
  6500,
  "Russian, English, Spanish"
);
const programmer2 = new Programmer("Ivan", 22, 1500, "Russian");

console.log(programmer);
console.log(programmer1.salary);
console.log(programmer2);
